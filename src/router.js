import Vue from "vue";
import Router from "vue-router";
import store from './store/index';

import Login from "./views/login.vue";
import Main from "./views/main.vue";

import Home from "./views/main/home.vue";
import Qwriter from "./views/main/qwriter.vue";
import Qbank from "./views/main/qbank.vue";
import Exam from "./views/main/exam.vue";
import Analysis from "./views/main/analysis.vue";
import Scoring from "./views/main/scoring.vue";
import OnlineExam from "./views/main/online-exam.vue";
import Qupdate from "./views/main/qupdate.vue";

Vue.use(Router);

const router = new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      component: Main,
      meta : {
        auth : true
      },
      redirect : {
        name : "qwriter"
      },
      children : [
        {
          path: "/home",
          name: "home",
          component: Home,
          meta : {
            auth : true
          }
        },
        {
          path: "/qwriter",
          name: "qwriter",
          component: Qwriter,
          meta : {
            auth : true
          }
        },
        {
          path: "/qbank",
          name: "qbank",
          component: Qbank,
          meta : {
            auth : true
          }
        },
        {
          path: "/exam",
          name: "exam",
          component: Exam,
          meta : {
            auth : true
          }
        },
        {
          path: "/analysis",
          name: "analysis",
          component: Analysis,
          meta : {
            auth : true
          }
        },
        {
          path: "/scoring",
          name: "scoring",
          component: Scoring,
          meta : {
            auth : true
          }
        },
        {
          path: "/online-exam",
          name: "online-exam",
          component: OnlineExam,
          meta : {
            auth : true
          }
        },
        {
          path: "/qupdate/:id",
          name: "qupdate",
          component: Qupdate,
          props: true,
          meta : {
            auth : true
          }
        }
      ]
    },
    {
      path: "/login",
      name: "login",
      component: Login
    },
    {
      path: "*",
      redirect: {
        name: "home"
      }
    }
  ]
});


router.beforeEach((to, from, next) => {
  // check meta auth
  if (to.matched.some(record => record.meta.auth)) {
    const auth = localStorage.getItem('auth');
    if (!auth) {
      //if no auth key, login page
      next('/login');
    } else {
      //cek validity
      store.commit('readUser');
      next();
    }
  } else {
    //if no meta auth, next page
    next();
  }
});

export default router;
