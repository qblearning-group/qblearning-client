module.exports = [
  {
    type: 'item',
    isHeader: true,
    name: 'MAIN NAVIGATION'
  },
  {
    type: 'item',
    icon: 'fa fa-pencil',
    name: 'Question Writer',
    router: {
      name: 'qwriter'
    }
  },
  {
    type: 'tree',
    icon: 'fa fa-database',
    name: 'Question Bank',
    router: {
      name: 'qbank'
    },
    items: [
      {
        type: 'item',
        icon: 'fa fa-circle-o',
        name: 'Quiz'
      },
      {
        type: 'item',
        icon: 'fa fa-circle-o',
        name: 'Exam'
      }
    ]
  },
  {
    type: 'item',
    icon: 'fa fa-gears',
    name: 'Exam Management',
    router: {
      name: 'exam'
    }
  },
  {
    type: 'item',
    icon: 'fa fa-search-plus',
    name: 'Analysis System',
    router: {
      name: 'analysis'
    }
  },
  {
    type: 'item',
    icon: 'fa fa-pie-chart',
    name: 'Scoring System',
    router: {
      name: 'scoring'
    }
  },
  {
    type: 'item',
    icon: 'fa fa-file-text-o',
    name: 'Online Exam',
    router: {
      name: 'online-exam'
    }
  },
  {
    type: 'item',
    name: 'Persons Module',
    isHeader: true
  },
  {
    type: 'item',
    icon: 'fa fa-circle-o text-red',
    name: 'Students',
  },
  {
    type: 'item',
    icon: 'fa fa-circle-o text-yellow',
    name: 'Lecturer',
  },
]
